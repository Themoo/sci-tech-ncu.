package com.nachiengmai.moo.sci_techncumobile;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

/**
 * Created by wacharapongnachiengmai on 8/1/15 AD.
 */
public class EventSciTechDAO {
    private SQLiteDatabase database;
    private DBHelper dbHelper;

    public EventSciTechDAO(Context context) {
        dbHelper = new DBHelper(context);
        database = dbHelper.getWritableDatabase();
    }

    public void close() {
        dbHelper.close();
    }

    public ArrayList<EventSciTech> getEventDate(Date date) {
        ArrayList<EventSciTech> eventDates = new ArrayList<>();
        String sqlText = "SELECT * FROM event_scitech WHERE event_date='" + dateToString(date) + "';";
        Cursor cursor = this.database.rawQuery(sqlText, null);
        cursor.moveToNext();
        EventSciTech eventSciTech;
        while (!cursor.isAfterLast()){
            eventSciTech = new EventSciTech();
            eventSciTech.setEventID(cursor.getInt(0));
            eventSciTech.setEventDate(cursor.getString(1));
            eventSciTech.setEventTitle(cursor.getString(2));
            eventSciTech.setEventType(cursor.getString(3));
            eventDates.add(eventSciTech);
            cursor.moveToNext();
        }
        cursor.close();
        return eventDates;
    }

    public HashMap<Date, Integer> loadEventToCalendar(){
        HashMap<Date, Integer> eventToCal = new HashMap<>();
        String sqlText = "SELECT * FROM event_scitech;";
        Cursor cursor = this.database.rawQuery(sqlText, null);
        cursor.moveToNext();
        while (!cursor.isAfterLast()){
            eventToCal.put(new Date(cursor.getString(1)), R.color.ncu_yellow);
            cursor.moveToNext();
        }
        cursor.close();
        return  eventToCal;
    }

    private String dateToString(Date date) {
        String dateString = "";
        String[] monthNameShort = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Nov", "Dec"};
        String dd, yy;
        int mm;
        SimpleDateFormat df1 = new SimpleDateFormat("dd");
        dd = df1.format(date);
        mm = date.getMonth();
        df1 = new SimpleDateFormat("yyyy");
        yy = df1.format(date);
        dateString = dd + "-" + monthNameShort[mm] + "-" + yy;
        Log.d("MooDate", dateString);
        return dateString;
    }
}
