package com.nachiengmai.moo.sci_techncumobile;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;

/**
 * Created by wacharapongnachiengmai on 7/18/15 AD.
 */
public class NewsDAO {
    private SQLiteDatabase database;
    private DBHelper dbHelper;

    public NewsDAO(Context context) {
        dbHelper = new DBHelper(context);
        database = dbHelper.getWritableDatabase();
    }

    public void close(){
        dbHelper.close();
    }

    public ArrayList<News> getAll(){
        ArrayList<News> allNews = new ArrayList<News>();
        Cursor cursor = this.database.rawQuery("SELECT * FROM news;",null);
        cursor.moveToFirst();
        News news;
        while (!cursor.isAfterLast()){
            news = new News();
            news.setNewsID(cursor.getInt(0));
            news.setNewsTitle(cursor.getString(1));
            news.setNewsContent(cursor.getString(2));
            news.setNewsDate(cursor.getString(3));
            news.setNewsIcon(cursor.getString(4));
            news.setNewsImage(cursor.getString(5));
            news.setNewsBody(cursor.getString(6));
            allNews.add(news);
            cursor.moveToNext();
        }
        cursor.close();
        return allNews;
    }

    public void add(News news) {
        ContentValues values = new ContentValues();
        //values.put("news_id", news.getNewsID());
        values.put("news_title", news.getNewsTitle());
        values.put("news_content", news.getNewsContent());
        values.put("news_date", news.getNewsDate());
        values.put("news_icon", news.getNewsIcon());
        values.put("news_image", news.getNewsImage());
        values.put("news_body", news.getNewsBody());
        this.database.insert("news", null, values);
        Log.d("Mooooooo:", "Add OK ;)");

    }

    public void deleteAll(){
        String sqlText = "DELETE FROM news;";
        this.database.execSQL(sqlText);
        Log.d("Moo", "Delete All Complete!!!");
    }
}
