package com.nachiengmai.moo.sci_techncumobile;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by wacharapongnachiengmai on 7/17/15 AD.
 */
public class DBHelper extends SQLiteOpenHelper {
    private static final String databaseName = "scitechncu.sqlite";
    private static final int databaseVersion = 2;
    Context myContext;

    public DBHelper(Context context) {
        super(context, databaseName, null, databaseVersion);
        this.myContext = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String sqlText = "CREATE TABLE lecturers (" +
                "lecturer_id TEXT PRIMARY KEY, " +
                "lecturer_name TEXT, " +
                "lecturer_position TEXT, " +
                "lecturer_picture TEXT, " +
                "lecturer_email TEXT, " +
                "lecturer_facebook TEXT, " +
                "lecturer_twitter TEXT" +
                ");";
        db.execSQL(sqlText);

        sqlText = "INSERT INTO lecturers (lecturer_id, lecturer_name, lecturer_position, lecturer_picture, lecturer_email, " +
                "lecturer_facebook, lecturer_twitter) " +
                "VALUES ('51001', 'รศ.วิไลพร สิริมังครารัตน์', 'คณบดีคณะวิทยาศาสตร์และเทคโนโลยี', 'personal'," +
                " 'wilaiporn@northcm.ac.th'," +
                " '-', '-');";
        db.execSQL(sqlText);
        sqlText = "INSERT INTO lecturers (lecturer_id, lecturer_name, lecturer_position, lecturer_picture, lecturer_email, " +
                "lecturer_facebook, lecturer_twitter) " +
                "VALUES ('51003', 'ดร. ศราวุธ แรมจันทร์', 'รองคณบดีคณะวิทยาศาสตร์และเทคโนโลยี', 'personal'," +
                " '-'," +
                " '-', '-');";
        db.execSQL(sqlText);
        sqlText = "INSERT INTO lecturers (lecturer_id, lecturer_name, lecturer_position, lecturer_picture, lecturer_email, " +
                "lecturer_facebook, lecturer_twitter) " +
                "VALUES ('51002', 'อรัญญา เวียนทอง', 'สาขาวิชาการจัดการเทคโนโลยีสารสนเทศและการสื่อสาร (ป.โท)', 'personal'," +
                " '-'," +
                " '-', '-');";
        db.execSQL(sqlText);
        sqlText = "INSERT INTO lecturers (lecturer_id, lecturer_name, lecturer_position, lecturer_picture, lecturer_email, " +
                "lecturer_facebook, lecturer_twitter) " +
                "VALUES ('51004', 'เชาวลิต เต็มปวน', 'หัวหน้าสาขาคอมพิวเตอร์ธุรกิจ', 'personal'," +
                " '-'," +
                " '-', '-');";
        db.execSQL(sqlText);
        sqlText = "INSERT INTO lecturers (lecturer_id, lecturer_name, lecturer_position, lecturer_picture, lecturer_email, " +
                "lecturer_facebook, lecturer_twitter) " +
                "VALUES ('51005', 'ณัฐกิตต์ ตรีวิทยากรานต์', 'อาจารย์ประจำสาขาคอมพิวเตอร์ธุรกิจ', 'personal'," +
                " '-'," +
                " '-', '-');";
        db.execSQL(sqlText);
        sqlText = "INSERT INTO lecturers (lecturer_id, lecturer_name, lecturer_position, lecturer_picture, lecturer_email, " +
                "lecturer_facebook, lecturer_twitter) " +
                "VALUES ('51006', 'นเรศ สุขร่องช้าง', 'อาจารย์ประจำสาขาคอมพิวเตอร์ธุรกิจ', 'personal'," +
                " '-'," +
                " '-', '-');";
        db.execSQL(sqlText);
        sqlText = "INSERT INTO lecturers (lecturer_id, lecturer_name, lecturer_position, lecturer_picture, lecturer_email, " +
                "lecturer_facebook, lecturer_twitter) " +
                "VALUES ('51007', 'รัฐนันท์ วุฒิเดช', 'อาจารย์ประจำสาขาคอมพิวเตอร์ธุรกิจ', 'personal'," +
                " '-'," +
                " '-', '-');";
        db.execSQL(sqlText);
        sqlText = "INSERT INTO lecturers (lecturer_id, lecturer_name, lecturer_position, lecturer_picture, lecturer_email, " +
                "lecturer_facebook, lecturer_twitter) " +
                "VALUES ('51008', 'ไอริณ สมบูรณ์', 'หัวหน้าสาขาวิชาการจัดการการพาณิชย์อิเล็กทรอนิกส์', 'personal'," +
                " '-'," +
                " '-', '-');";
        db.execSQL(sqlText);
        sqlText = "INSERT INTO lecturers (lecturer_id, lecturer_name, lecturer_position, lecturer_picture, lecturer_email, " +
                "lecturer_facebook, lecturer_twitter) " +
                "VALUES ('53005', 'อำพล กองเขียว', 'หัวหน้าสาขาวิชาวิศวกรรมซอฟต์แวร์', 'personal'," +
                " '-'," +
                " '-', '-');";
        db.execSQL(sqlText);
        sqlText = "INSERT INTO lecturers (lecturer_id, lecturer_name, lecturer_position, lecturer_picture, lecturer_email, " +
                "lecturer_facebook, lecturer_twitter) " +
                "VALUES ('51009', 'ธีรัช สายชู', 'ประธานหลักสูตรสาขาวิชาวิศวกรรมซอฟต์แวร์', 'personal'," +
                " '-'," +
                " '-', '-');";
        db.execSQL(sqlText);
        sqlText = "INSERT INTO lecturers (lecturer_id, lecturer_name, lecturer_position, lecturer_picture, lecturer_email, " +
                "lecturer_facebook, lecturer_twitter) " +
                "VALUES ('51010', 'ดนุพล วันชัยสถิร', 'อาจารย์ประจำสาขาวิชาวิศวกรรมซอฟต์แวร์', 'personal'," +
                " '-'," +
                " '-', '-');";
        db.execSQL(sqlText);
        sqlText = "INSERT INTO lecturers (lecturer_id, lecturer_name, lecturer_position, lecturer_picture, lecturer_email, " +
                "lecturer_facebook, lecturer_twitter) " +
                "VALUES ('51011', 'รุ่งฤทธิ์ อนุตรวิรามกุล', 'อาจารย์ประจำสาขาวิชาวิศวกรรมซอฟต์แวร์', 'personal'," +
                " '-'," +
                " '-', '-');";
        db.execSQL(sqlText);

        sqlText = "INSERT INTO lecturers (lecturer_id, lecturer_name, lecturer_position, lecturer_picture, lecturer_email, " +
                "lecturer_facebook, lecturer_twitter) " +
                "VALUES ('53006', 'วัชรพงษ์ ณ เชียงใหม่', 'อาจารย์ประจำสาขาวิชาวิศวกรรมซอฟต์แวร์', 'wacharapong'," +
                " 'wacharapong@northcm.ac.th'," +
                " 'TheMoo Nachiengmai', 'aj_moo');";
        db.execSQL(sqlText);

        //V2
        sqlText = "CREATE TABLE news (" +
                "news_id INTEGER PRIMARY KEY, " +
                "news_title TEXT, " +
                "news_content TEXT, " +
                "news_date TEXT, " +
                "news_icon TEXT, " +
                "news_image TEXT, " +
                "news_body TEXT" +
                ");";
        db.execSQL(sqlText);

        sqlText = "INSERT INTO news (news_title, news_content, news_date, " +
                "news_icon, news_image, news_body) " +
                "VALUES ('News Example 1', 'ทดสอบแจ้งข่าว 1 สาขาวิชาวิศวกรรมซอฟต์แวร์', '18 กรกฎาคม 2558', 'noname', 'img1', 'news body');";
        db.execSQL(sqlText);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        String sqlText = "CREATE TABLE event_scitech (" +
                "event_id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                "event_date TEXT, " +
                "event_title TEXT, " +
                "event_type TEXT" +
                ");";
        db.execSQL(sqlText);
        sqlText = "INSERT INTO event_scitech (" +
                "event_date," +
                "event_title, " +
                "event_type" +
                ") VALUES (" +
                "'10-Aug-2015', " +
                "'Event Test 1', " +
                "'1'" +
                ");";
        db.execSQL(sqlText);
        sqlText = "INSERT INTO event_scitech (" +
                "event_date," +
                "event_title, " +
                "event_type" +
                ") VALUES (" +
                "'14-Aug-2015', " +
                "'Event Test 2', " +
                "'1'" +
                ");";
        db.execSQL(sqlText);
        sqlText = "INSERT INTO event_scitech (" +
                "event_date," +
                "event_title, " +
                "event_type" +
                ") VALUES (" +
                "'26-Aug-2015', " +
                "'Event Test 3', " +
                "'1'" +
                ");";
        db.execSQL(sqlText);
    }
}
