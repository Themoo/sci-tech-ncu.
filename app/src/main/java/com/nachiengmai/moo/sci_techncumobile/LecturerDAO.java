package com.nachiengmai.moo.sci_techncumobile;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

/**
 * Created by wacharapongnachiengmai on 7/17/15 AD.
 */
public class LecturerDAO {
    private SQLiteDatabase database;
    private DBHelper dbHelper;

    public LecturerDAO(Context context) {
        dbHelper = new DBHelper(context);
        database = dbHelper.getWritableDatabase();
    }

    public void close(){
        this.dbHelper.close();
    }

    public ArrayList<Lecturer> getAll() {
        ArrayList<Lecturer> lecturers = new ArrayList<Lecturer>();
        Cursor cursor = this.database.rawQuery("SELECT * FROM lecturers;",null);
        cursor.moveToFirst();
        Lecturer lecturer;
        while (!cursor.isAfterLast()){
            lecturer = new Lecturer();
            lecturer.setLecID(cursor.getString(0));
            lecturer.setLecName(cursor.getString(1));
            lecturer.setLecPosition(cursor.getString(2));
            lecturer.setLecPicture(cursor.getString(3));
            lecturer.setLecEmail(cursor.getString(4));
            lecturer.setLecFacebook(cursor.getString(5));
            lecturer.setLecTwitter(cursor.getString(6));
            lecturers.add(lecturer);
            cursor.moveToNext();
        }
        cursor.close();
        return lecturers;
    }
}
