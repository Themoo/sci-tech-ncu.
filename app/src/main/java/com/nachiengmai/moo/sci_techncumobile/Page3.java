package com.nachiengmai.moo.sci_techncumobile;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

/**
 * Created by wacharapongnachiengmai on 6/28/15 AD.
 */
public class Page3 extends Fragment {
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.page3,container,false);

        ListView lecturerListView = (ListView)v.findViewById(R.id.lecturer_listview);

//      Setup LecturerArrayListData
        LecturerDAO lecturerDAO = new LecturerDAO(v.getContext());

        LecturerList adapter = new LecturerList(this.getActivity(), lecturerDAO.getAll());
        lecturerListView.setAdapter(adapter);

        lecturerDAO.close();

        return v;
    }

}
