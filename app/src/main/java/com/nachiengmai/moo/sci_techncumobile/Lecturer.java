package com.nachiengmai.moo.sci_techncumobile;

import java.io.Serializable;

/**
 * Created by wacharapongnachiengmai on 7/17/15 AD.
 */
public class Lecturer implements Serializable {
    private String lecID;
    private String lecName;
    private String lecPosition;
    private String lecPicture;
    private String lecEmail;
    private String lecFacebook;
    private String lecTwitter;

    public Lecturer() {
    }

    public String getLecPicture() {
        return lecPicture;
    }

    public void setLecPicture(String lecPicture) {
        this.lecPicture = lecPicture;
    }

    public String getLecID() {
        return lecID;
    }

    public void setLecID(String lecID) {
        this.lecID = lecID;
    }

    public String getLecName() {
        return lecName;
    }

    public void setLecName(String lecName) {
        this.lecName = lecName;
    }

    public String getLecPosition() {
        return lecPosition;
    }

    public void setLecPosition(String lecPosition) {
        this.lecPosition = lecPosition;
    }

    public String getLecEmail() {
        return lecEmail;
    }

    public void setLecEmail(String lecEmail) {
        this.lecEmail = lecEmail;
    }

    public String getLecFacebook() {
        return lecFacebook;
    }

    public void setLecFacebook(String lecFacebook) {
        this.lecFacebook = lecFacebook;
    }

    public String getLecTwitter() {
        return lecTwitter;
    }

    public void setLecTwitter(String lecTwitter) {
        this.lecTwitter = lecTwitter;
    }
}
