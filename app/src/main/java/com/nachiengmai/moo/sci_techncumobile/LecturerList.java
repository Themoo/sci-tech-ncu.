package com.nachiengmai.moo.sci_techncumobile;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by wacharapongnachiengmai on 7/17/15 AD.
 */
public class LecturerList extends BaseAdapter {
    private static Activity activity;
    private static LayoutInflater inflater;
    ArrayList<Lecturer> lecturersArray;

    public LecturerList(Activity activity, ArrayList<Lecturer> lecturersArray) {
        this.activity = activity;
        this.lecturersArray = lecturersArray;
        inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return this.lecturersArray.size();
    }

    @Override
    public Lecturer getItem(int position) {
        return this.lecturersArray.get(position);
    }

    @Override
    public long getItemId(int position) {
        return Long.parseLong(this.lecturersArray.get(position).getLecID());
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        v = inflater.inflate(R.layout.personal_layout, null);
        TextView nameText = (TextView)v.findViewById(R.id.text_name);
        nameText.setText(this.lecturersArray.get(position).getLecName());
        TextView positionText = (TextView)v.findViewById(R.id.text_position);
        positionText.setText(this.lecturersArray.get(position).getLecPosition());
        TextView emailText = (TextView)v.findViewById(R.id.text_email);
        emailText.setText(this.lecturersArray.get(position).getLecEmail());
        TextView facebookText = (TextView)v.findViewById(R.id.text_facebook);
        facebookText.setText(this.lecturersArray.get(position).getLecFacebook());
        TextView twitterText = (TextView)v.findViewById(R.id.text_twitter);
        twitterText.setText(this.lecturersArray.get(position).getLecTwitter());
        ImageView imageView = (ImageView)v.findViewById(R.id.personal_pic);
            Resources resources = v.getResources();
            int resID = resources.getIdentifier(this.lecturersArray.get(position).getLecPicture(), "drawable",
                    v.getContext().getPackageName());
            Drawable drawable = resources.getDrawable(resID);
            imageView.setImageDrawable(drawable);
        return v;
    }
}
