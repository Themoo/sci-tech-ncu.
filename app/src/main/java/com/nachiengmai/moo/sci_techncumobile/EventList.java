package com.nachiengmai.moo.sci_techncumobile;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by wacharapongnachiengmai on 8/2/15 AD.
 */
public class EventList extends BaseAdapter {
    private static Activity activity;
    private static LayoutInflater inflater;
    ArrayList<EventSciTech> eventArray;

    public EventList(Activity activity, ArrayList<EventSciTech> eventArray) {
        this.activity = activity;
        this.eventArray = eventArray;
        inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override



    public int getCount() {
        return eventArray.size();
    }

    @Override
    public EventSciTech getItem(int position) {
        return eventArray.get(position);
    }

    @Override
    public long getItemId(int position) {
        return eventArray.get(position).getEventID();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        v = inflater.inflate(R.layout.event_layout, null);
        TextView textView = (TextView)v.findViewById(R.id.event_text);
        textView.setText(eventArray.get(position).getEventTitle());
        return v;
    }
}
