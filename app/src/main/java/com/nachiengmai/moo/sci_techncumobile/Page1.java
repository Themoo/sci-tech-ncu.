package com.nachiengmai.moo.sci_techncumobile;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

/**
 * Created by wacharapongnachiengmai on 7/18/15 AD.
 */
public class Page1 extends Fragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.page1,container,false);

        ListView newsListView = (ListView)v.findViewById(R.id.news_listview);

        NewsDAO newsDAO = new NewsDAO(v.getContext());

        NewsList adapter = new NewsList(this.getActivity(), newsDAO.getAll());
        newsListView.setAdapter(adapter);
        return v;
    }
}
