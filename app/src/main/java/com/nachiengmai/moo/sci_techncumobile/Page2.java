package com.nachiengmai.moo.sci_techncumobile;

import android.app.FragmentTransaction;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CalendarView;
import android.widget.ListView;
import android.widget.Toast;

import com.roomorama.caldroid.CaldroidFragment;
import com.roomorama.caldroid.CaldroidListener;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;

/**
 * Created by wacharapongnachiengmai on 7/26/15 AD.
 */
public class Page2 extends Fragment {
    CalendarView myCalendar;
    ListView eventList;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View v = inflater.inflate(R.layout.page2, container, false);
        final CaldroidFragment caldroidFragment = new CaldroidFragment();
        Bundle args = new Bundle();
        Calendar cal = Calendar.getInstance();
        args.putInt(CaldroidFragment.MONTH, cal.get(Calendar.MONTH) + 1);
        args.putInt(CaldroidFragment.YEAR, cal.get(Calendar.YEAR));
        args.putInt(CaldroidFragment.THEME_RESOURCE, R.style.CaldroidDefaultNCU);
        caldroidFragment.setArguments(args);

        android.support.v4.app.FragmentTransaction t = getActivity().getSupportFragmentManager().beginTransaction();
        t.replace(R.id.calendar1, caldroidFragment);
        t.commit();

        EventSciTechDAO eventDAO = new EventSciTechDAO(getActivity().getApplicationContext());

        caldroidFragment.setBackgroundResourceForDates(eventDAO.loadEventToCalendar());

        caldroidFragment.refreshView();

        eventList = (ListView)v.findViewById(R.id.event_list);
        EventList adapter = new EventList(getActivity(), eventDAO.getEventDate(new Date()));
        eventList.setAdapter(adapter);
        eventDAO.close();

        final SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
        final Date[] selectedDate = {new Date()};
        final CaldroidListener listener = new CaldroidListener() {

            @Override
            public void onSelectDate(Date date, View view) {
                EventSciTechDAO eventSciTechDAO = new EventSciTechDAO(getActivity().getApplicationContext());
                EventList adapter = new EventList(getActivity(), eventSciTechDAO.getEventDate(date));
                eventList.setAdapter(adapter);
                eventSciTechDAO.close();
//                Toast.makeText(getActivity().getApplicationContext(), formatter.format(date),
//                        Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onChangeMonth(int month, int year) {
//                String text = "month: " + month + " year: " + year;
//                Toast.makeText(getActivity().getApplicationContext(), text,
//                        Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onLongClickDate(Date date, View view) {
//                Toast.makeText(getActivity().getApplicationContext(),
//                        "Long click " + formatter.format(date),
//                        Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onCaldroidViewCreated() {
//                Toast.makeText(getActivity().getApplicationContext(),
//                        "Caldroid view is created",
//                        Toast.LENGTH_SHORT).show();
            }

        };
        caldroidFragment.setCaldroidListener(listener);

        return v;
    }

    private String dateToString(Date date) {
        String dateString = "";
        String[] monthNameShort = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Nov", "Dec"};
        String dd, yy;
        int mm;
        SimpleDateFormat df1 = new SimpleDateFormat("dd");
        dd = df1.format(date);
        mm = date.getMonth();
        df1 = new SimpleDateFormat("yyyy");
        yy = df1.format(date);
        dateString = dd + "-" + monthNameShort[mm] + "-" + yy;
        Log.d("MooDate", dateString);
        return dateString;
    }

}
