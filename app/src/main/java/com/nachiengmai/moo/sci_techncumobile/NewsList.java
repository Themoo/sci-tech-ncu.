package com.nachiengmai.moo.sci_techncumobile;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

/**
 * Created by wacharapongnachiengmai on 7/18/15 AD.
 */
public class NewsList extends BaseAdapter {
    private static Activity activity;
    private static LayoutInflater inflater;
    ArrayList<News> newsArray;

    public NewsList(Activity activity, ArrayList<News> newsArray) {
        this.activity = activity;
        this.newsArray = newsArray;
        inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return newsArray.size();
    }

    @Override
    public News getItem(int position) {
        return newsArray.get(position);
    }

    @Override
    public long getItemId(int position) {
        return newsArray.get(position).getNewsID();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        v = inflater.inflate(R.layout.news_layout, null);
        TextView newsTitle = (TextView)v.findViewById(R.id.news_title_text);
        TextView newsContent = (TextView)v.findViewById(R.id.news_content_text);
        TextView newsDate = (TextView)v.findViewById(R.id.new_date_text);
        newsTitle.setText(newsArray.get(position).getNewsTitle());
        newsContent.setText(newsArray.get(position).getNewsContent());
        newsDate.setText(newsArray.get(position).getNewsDate());
        ImageView imgIcon = (ImageView)v.findViewById(R.id.icon_img);
        new DownLoadImage(imgIcon).execute("http://www.nachiengmai.net/scitech/"+
                newsArray.get(position).getNewsIcon());
        return v;
    }

    private class DownLoadImage extends AsyncTask<String, Void, Bitmap> {

        ImageView imageView;

        public DownLoadImage(ImageView imageView) {
            this.imageView = imageView;
        }

        @Override
        protected Bitmap doInBackground(String... urls) {
            Bitmap myBitmap = null;
            String urld = urls[0];
            try {
                InputStream inputStream = new URL(urld).openStream();
                myBitmap = BitmapFactory.decodeStream(inputStream);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return myBitmap;
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            imageView.setImageBitmap(bitmap);
        }
    }
}
