package com.nachiengmai.moo.sci_techncumobile;

import java.io.Serializable;

/**
 * Created by wacharapongnachiengmai on 8/1/15 AD.
 */
public class EventSciTech implements Serializable {
    private int eventID;
    private String eventDate;
    private String eventTitle;
    private String eventType;

    public int getEventID() {
        return eventID;
    }

    public void setEventID(int eventID) {
        this.eventID = eventID;
    }

    public String getEventDate() {
        return eventDate;
    }

    public void setEventDate(String eventDate) {
        this.eventDate = eventDate;
    }

    public String getEventTitle() {
        return eventTitle;
    }

    public void setEventTitle(String eventTitle) {
        this.eventTitle = eventTitle;
    }

    public String getEventType() {
        return eventType;
    }

    public void setEventType(String eventType) {
        this.eventType = eventType;
    }
}
